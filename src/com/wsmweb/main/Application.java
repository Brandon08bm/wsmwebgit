package com.wsmweb.main;

import static spark.Spark.delete;
import static spark.Spark.post;
import static spark.Spark.put;

import com.wsmweb.controller.DeleteController;
import com.wsmweb.controller.GetController;
import com.wsmweb.controller.PostController;
import com.wsmweb.controller.PutController;

import spark.servlet.SparkApplication;

public class Application implements SparkApplication{
	@Override
	public void init() {
		post("/:nty" , "application/json",(req, res) -> {return GetController.control(req, res);});	
		post("/:nty" , "application/json",(req, res) -> {return PostController.control(req, res);});
		put("/:nty" , "application/json",(req, res) -> {return PutController.control(req, res);});
		delete("/:nty" , "application/json",(req, res) -> {return DeleteController.control(req, res);});
	}
}
