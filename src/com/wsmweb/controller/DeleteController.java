package com.wsmweb.controller;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wsmweb.entity.Bodega;
import com.wsmweb.entity.FilterRequest;
import com.wsmweb.home.BodegaHome;
import com.wsmweb.utils.JsonUtil;
import com.wsmweb.utils.Message;

import spark.Request;
import spark.Response;

public class DeleteController {

public static Object control(Request req, Response res) {
		try {
			//modificar los metos 
			String NTY = req.params(":nty");
			ObjectMapper mapper;
			FilterRequest input;
			/*TODO 01 bodega*/
			if(NTY.trim().equals("01")) {
				//Cambiar el metodo finall por el delete
				mapper  = new ObjectMapper();
				input = mapper.readValue(req.body(), FilterRequest.class);
				BodegaHome dao = new BodegaHome();
				List<Bodega> bodega= dao.findAll(input.getField(), input.getValue(), input.getState(), true);
				if (bodega != null && bodega.size()>0) {
					return JsonUtil.dataToJson(bodega);
				}
				return JsonUtil.dataToJson(new Message(500, "NO_FOUND", "No se encontraron reultados", input.getValue()));
			}
			
			return JsonUtil.dataToJson(new Message(400, "ERROR", "Servicio no existe"));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return JsonUtil.dataToJson(new Message(400, "ERROR", e.getMessage()));
		}
	}
}
