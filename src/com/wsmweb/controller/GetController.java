package com.wsmweb.controller;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wsmweb.entity.Bodega;
import com.wsmweb.entity.FilterRequest;
import com.wsmweb.entity.Linea;
import com.wsmweb.entity.Marca;
import com.wsmweb.entity.Pais;
import com.wsmweb.home.BodegaHome;
import com.wsmweb.home.LineaHome;
import com.wsmweb.home.MarcaHome;
import com.wsmweb.home.PaisHome;
import com.wsmweb.utils.JsonUtil;
import com.wsmweb.utils.Message;

import spark.Request;
import spark.Response;


public class GetController {

	private GetController() {
	}

	public static Object control(Request req, Response res) {
		try {
			String NTY = req.params(":nty");
			ObjectMapper mapper;
			FilterRequest input;
			/*TODO 01 bodega*/
			if(NTY.trim().equals("01")) {
				mapper  = new ObjectMapper();
				input = mapper.readValue(req.body(), FilterRequest.class);
				BodegaHome dao = new BodegaHome();
				List<Bodega> bodega= dao.findAll(input.getField(), input.getValue(), input.getState(), true);
				if (bodega != null && bodega.size()>0) {
					return JsonUtil.dataToJson(bodega);
				}
				return JsonUtil.dataToJson(new Message(500, "NO_FOUND", "No se encontraron reultados", input.getValue()));
			}
			/*TODO 02 Linea*/
			if(NTY.trim().equals("02")){
				mapper  = new ObjectMapper();
				input = mapper.readValue(req.body(), FilterRequest.class);
				LineaHome dao = new LineaHome();
				List<Linea> linea = dao.findAll(input.getField(), input.getValue(), input.getState(), true);
				if (linea != null && linea.size()>0) {
					return JsonUtil.dataToJson(linea);
				}
				return JsonUtil.dataToJson(new Message(500, "NO_FOUND", "No se encontraron reultados", input.getValue()));
			}
			/*TODO 03 Marca*/
			if(NTY.trim().equals("03")){
				mapper  = new ObjectMapper();
				input = mapper.readValue(req.body(), FilterRequest.class);
				MarcaHome dao = new MarcaHome();
				List<Marca> marca = dao.findAll(input.getField(), input.getValue(), input.getState(), true);
				if (marca != null && marca.size()>0) {
					return JsonUtil.dataToJson(marca);
				}
				return JsonUtil.dataToJson(new Message(500, "NO_FOUND", "No se encontraron reultados", input.getValue()));
			}
			/*TODO 04 Pais.*/
			if(NTY.trim().equals("04")){
				mapper  = new ObjectMapper();
				input = mapper.readValue(req.body(), FilterRequest.class);
				PaisHome dao = new PaisHome();
				List<Pais> pais = dao.findAll(input.getField(), input.getValue(), input.getState(), true);
				if (pais != null && pais.size()>0) {
					return JsonUtil.dataToJson(pais);
				}
				return JsonUtil.dataToJson(new Message(500, "NO_FOUND", "No se encontraron reultados", input.getValue()));
			}
			return JsonUtil.dataToJson(new Message(400, "ERROR", "Servicio no existe"));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
			return JsonUtil.dataToJson(new Message(400, "ERROR", e.getMessage()));
		}
	}
}
