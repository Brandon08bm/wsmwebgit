package com.wsmweb.utils;

public class Message {

	private int code;
	private String status;
	private String msg;

	public Message() {}
	
	public Message(int code, String status, String msg, String... args) {
		this.code = code;
		this.status = status;
		this.msg = String.format(msg, args);
	}

	public Message(Exception e) {
		this.msg = e.getMessage();
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg, String... args) {
		this.msg = String.format(msg, args);
	}
}