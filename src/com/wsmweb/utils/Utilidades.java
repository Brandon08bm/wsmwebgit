package com.wsmweb.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import org.hibernate.Session;

public class Utilidades {

	// QC remplazar caracteres
	public String replace(String str, String pattern, String replace) {
		StringBuffer result = new StringBuffer();
		if (str != null) {
			if (str.length() > 0) {
				int s = 0;
				int e = 0;
				while ((e = str.indexOf(pattern, s)) >= 0) {
					result.append(str.substring(s, e));
					result.append(replace);
					s = e + pattern.length();
				}
				result.append(str.substring(s));
			}
		}
		return result.toString();
	}

	public static final String formato_ingreso(String str) {
		String retorna = "";
		if (str != null && str.length() > 0) {
			String POSICION = "";
			String VAL1 = "";
			int tclave = 0;
			tclave = str.trim().length();
			String cadena = cadena_ing();

			for (int i = 0; i < tclave; i++) {
				VAL1 = str.substring(i, i + 1);
				int pos = ((cadena.lastIndexOf(VAL1) + 1) * 5);
				POSICION = pos + "O";
				retorna = retorna + POSICION;
			}

		}
		retorna = retorna.trim();
		return retorna;
	}

	public String elim_formato(String str) {
		String result = str.replace(",", "");
		result = result.replace("$", "");
		result = result.replace(".", "");
		result = result.trim();
		return result.toString();
	}

	public String URL_formato(String str) {
		String result = str.replace("%", "");
		result = result.replace("$", "");
		result = result.replace("/", "");
		result = result.replace(" ", "_");
		result = result.replace("&", "");
		result = result.replace("#", "");
		result = result.trim();
		return result.toString();
	}

	// QC formato de comas para valores
	public String Formatocomas(String str) {
		String result = "";
		if (str.length() > 3) {
			if (str.length() < 7) {
				result = str.substring(0, str.length() - 3) + "."
						+ str.substring(str.length() - 3, str.length());
			} else {
				if (str.length() > 6) {
					result = str.substring(0, str.length() - 6) + "."
							+ str.substring(str.length() - 6, str.length() - 3)
							+ "."
							+ str.substring(str.length() - 3, str.length());
				}
			}
		} else {
			result = str;
		}
		result = result.trim();
		return result.toString();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static String[] split(String original, String separator) {
		Vector nodes = new Vector();
		// Parse nodes into vector
		int index = original.indexOf(separator);
		while (index >= 0) {
			nodes.addElement(original.substring(0, index));
			original = original.substring(index + separator.length());
			index = original.indexOf(separator);
		}
		// Get the last node
		nodes.addElement(original);

		// Create split string array
		String[] result = new String[nodes.size()];
		if (nodes.size() > 0) {
			for (int loop = 0; loop < nodes.size(); loop++) {
				result[loop] = (String) nodes.elementAt(loop);
				// System.out.println(result[loop]);
			}

		}
		return result;
	}
	
	public String llenar(String variable, String relleno, int largo,
			String dir) {
		int val_o = variable.length();
		while (val_o < largo) {
			if (dir.equals("de")) {
				variable = variable + relleno;
			} else {
				variable = relleno + variable;
			}
			val_o++;
		}
		return variable;
	}

	public String centrar(String variable, String relleno, int largo) {
		int llenar = (largo-variable.length())/2;
		for (int i = 0; i < llenar; i++) {
			variable = variable + relleno;
		}
		for (int j = 0; j < llenar; j++) {
			variable = relleno + variable;
		}

		return variable;
	}

	public String getDateTimeString() {
		String fechahora = new SimpleDateFormat("yyyy-MM-dd HH:mm aa")
				.format(Calendar.getInstance().getTime());
		return fechahora;
	}

	public static String getDateString() {
		String fecha = new SimpleDateFormat("yyyy-MM-dd").format(Calendar
				.getInstance().getTime());
		return fecha;
	}

	public String getSalto_Linea(String val) {
		String res = "\n";
		if (val.equals("1")) {
			res = "\n";
		}
		if (val.equals("2")) {
			res = "\r";
		}
		return res;
	}

	public static final String cadena_ing() {
		return "4pq1rsAJtBu2vwCxQy3DE5zF6G7H8I90KL/M*N-�+OZaPbcdeRfSgThUiVjklmWnX�Yo";

	}



	public static void deleteAllFiles(File directorio) {
		File[] ficheros = directorio.listFiles();
		for (int i=0; i < ficheros.length; i++){
			if (ficheros[i].isDirectory()) {
				deleteAllFiles(ficheros[i]);
			}
			ficheros[i].delete();
		}
	}

	public static void moveFile(String source, String dest) {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(new File(source));
			os = new FileOutputStream(new File(dest));
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			is.close();
			os.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public static boolean renameFile(String fileSource, String fileTarget) {
		File file = new File(fileSource);
		if(file.isFile()){
			file.renameTo(new File(fileTarget));
			return true;
		}
		else {
			System.out.println(file.getName()+" No es un archivo ");
			return false;
		} 
	}

	public static boolean existFile(String filePath) {
		try {
			File f = new File(filePath);
			if(f.exists() && !f.isDirectory()) { 
				return true;
			}
			return false;
		}
		catch(Exception e) {
			return false;
		}
	}

	private static String formatFields(String input) {
		return format(input,"CAST("," AS VARCHAR)");
	}
	private static String formatValues(String input) {
		return format(input,"'","'");
	}
	private static String format(String input, String tagBegin, String tagEnd) {
		String outputStr = "";
		String [] split = input.split(",");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < split.length; i++) {
			sb.append(tagBegin + split[i].trim() + tagEnd);
			if (i != (split.length - 1)) {
				sb.append("||");
			}
		}
		outputStr = sb.toString();
		return outputStr;
	}
	@SuppressWarnings({ "rawtypes" })
	public static boolean existeRelacion(String fields, String values, String table){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		String queryString =
				"SELECT " + fields + " FROM " + schema + "." + table;
		if((fields != "" && fields != null) && (values != "" && values != null)) {
			fields = formatFields(fields);
			values = formatValues(values);
			queryString += " WHERE " + fields + " = " + values;
		}
		try {
			System.out.println("QUERY := "+queryString);
			List result = (List) session.createSQLQuery(queryString).list();
			session.close();
			if (result != null && result.size() > 0) {
				return true;
			}
			return false;
		} catch (RuntimeException re) {
			session.close();
			throw re;
		}
	}

	@SuppressWarnings({ "rawtypes" })
	public static boolean existeRelacionLike(String fields, String values, String table){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		String queryString =
				"SELECT " + fields + " FROM " + schema + "." + table;
		if((fields != "" && fields != null) && (values != "" && values != null)) {
			fields = formatFields(fields);

			queryString += " WHERE " + fields + " LIKE '%"+values+"%'";
		}
		try {
			System.out.println("QUERY := "+queryString);
			List result = (List) session.createSQLQuery(queryString).list();
			session.close();
			if (result != null && result.size() > 0) {
				return true;
			}
			return false;
		} catch (RuntimeException re) {
			session.close();
			throw re;
		}
	}

	public static Date getPrimerDiaDelMes() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH),
				cal.getActualMinimum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY),
				cal.getMinimum(Calendar.MINUTE),
				cal.getMinimum(Calendar.SECOND));
		return cal.getTime();
	}

	public static Date getPrimerDiaDelMes(Date fecha) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		cal.set(cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH),
				cal.getActualMinimum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY),
				cal.getMinimum(Calendar.MINUTE),
				cal.getMinimum(Calendar.SECOND));
		return cal.getTime();
	}

	public static Date getUltimoDiaDelMes() {
		Calendar cal = Calendar.getInstance();
		cal.set(cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH),
				cal.getActualMaximum(Calendar.DAY_OF_MONTH),
				cal.getMinimum(Calendar.HOUR_OF_DAY),
				cal.getMinimum(Calendar.MINUTE),
				cal.getMinimum(Calendar.SECOND));
		return cal.getTime();
	}

	/*	public static String getPropertie(String p) {
		return LoaderResourceElements.getInstance().getParameters(p, "parameters.properties");
	}

	public static String getPropertieMsg(String p) {
		return LoaderResourceElements.getInstance().getParameters(p, "messages.properties");
	}
	*/

	public static String removeEnd(String s) {
		if(s != null && s.length() > 0) 
			return s.substring(0, s.length()-1);
		return "";
	}

	public static String capitalized(String text){
		text.toLowerCase();
		if(text!=null && !text.equals("")){
			text = text.substring(0,1).toUpperCase() + text.substring(1);
		}
		return text;
	}


}
