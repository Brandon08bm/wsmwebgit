package com.wsmweb.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wsmweb.entity.Bodega;
import com.wsmweb.entity.Marca;
import com.wsmweb.utils.HibernateUtil;

public class MarcaHome {

	private String tabla=".marca";


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Marca findBy(String field, String value,String state){
		Marca resultado = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT marid, marcodigo, marnombre, marestado, "
					+ " marusucrea, marusumod,  marfeccrea, marfecmod FROM "+schema+tabla;
			String where ="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				where += " AND "+field.trim() + " = :p1";
				properties.put("p1", value);
			}
			if(state != null && !state.equals("")){
				where += " AND marestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString += (where.startsWith(" AND "))? " WHERE "+ where.substring(4): " WHERE " + where;
			}
			queryString +=" LIMIT 1";
			List result = (List<Bodega>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				while(((Iterator)resIterator).hasNext()){
					resultado = new Marca();
					Object[] row = (Object[])resIterator.next();

					Integer marid = (Integer) row[0];
					String marcodigo = (String) row[1];
					String marnombre = (String) row[2];
					Boolean marestado = ((Short) row[3]).intValue()==1;
					String marusucrea = (String) row[4];
					String marusumod = (String) row[5];
					Date marfeccrea = (Date) row[6];
					Date marfecmod = (Date) row[7];

					resultado.setMarid(marid);
					resultado.setMarcodigo(marcodigo.trim());
					resultado.setMarnombre(marnombre.trim());
					resultado.setMarestado(marestado);
					resultado.setMarusucrea(marusucrea.trim());
					resultado.setMarusumod(marusumod.trim());
					resultado.setMarfeccrea(marfeccrea);
					resultado.setMarfecmod(marfecmod);

				}
			}
			session.close();
			return resultado;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Marca> findAll(String field, String value,String state,boolean like){
		List<Marca> lista = new ArrayList<Marca>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT marid, marcodigo, marnombre, marestado, "
					+ " marusucrea, marusumod,  marfeccrea, marfecmod FROM "+schema+tabla;
			String where="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				if(like){
					where += " AND "+field.trim()+" LIKE :p1";
					properties.put("p1", "%"+value+"%");
				}else{
					where += " AND "+field.trim()+"= :p1";
					properties.put("p1", value);
				}
			}
			if(state != null && !state.equals("")){
				where += " AND marestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString +=(where.startsWith(" AND "))?" WHERE "+where.substring(4):" WHERE "+where;
			}
			if(field != null && !field.trim().equals("")){
				queryString += " ORDER BY "+ field.trim();
			}else{
				queryString += " ORDER BY marnombre";
			}
			List result = (List<Marca>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				Marca resultado = null;
				while(((Iterator)resIterator).hasNext()){
					resultado = new Marca();
					Object[] row = (Object[])resIterator.next();

					Integer marid = (Integer) row[0];
					String marcodigo = (String) row[1];
					String marnombre = (String) row[2];
					Boolean marestado = ((Short) row[3]).intValue()==1;
					String marusucrea = (String) row[4];
					String marusumod = (String) row[5];
					Date marfeccrea = (Date) row[6];
					Date marfecmod = (Date) row[7];

					resultado.setMarid(marid);
					resultado.setMarcodigo(marcodigo);
					resultado.setMarnombre(marnombre);
					resultado.setMarestado(marestado);
					resultado.setMarusucrea(marusucrea);
					resultado.setMarusumod(marusumod);
					resultado.setMarfeccrea(marfeccrea);
					resultado.setMarfecmod(marfecmod);

					lista.add(resultado);
				}
			}
			session.close();
			return lista;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	public void save(Marca obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" INSERT INTO "+schema+tabla
					+ "( marcodigo, marnombre, marestado, marusucrea, marusumod, marfeccrea, marfecmod)"
					+ " VALUES ( "
					+"'"+obj.getMarcodigo().toUpperCase()+"',"
					+"'"+obj.getMarnombre().toUpperCase()+"',"
					+(obj.getMarestado()?1:0)+","
					+"'"+ usuario+"',"
					+"'"+ usuario +"',"
					+ " now(),"
					+ " now() )";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void update(Marca obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" UPDATE "+schema+tabla
					+" SET marnombre='"+obj.getMarnombre().toUpperCase()+"'," 
					+" marestado ="+(obj.getMarestado()?1:0)+"," 
					+" marusumod= '"+usuario+"',"
					+" marfecmod= now() "
					+" WHERE marid="+obj.getMarid()
					+ " AND marcodigo='"+obj.getMarcodigo().toUpperCase()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void delete(Marca obj){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString= " DELETE FROM " +schema+tabla
					+" WHERE marid="+obj.getMarid()
					+ " AND marcodigo='"+obj.getMarcodigo().toUpperCase()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		} catch (RuntimeException e) {
			session.close();
		}
	}
}
