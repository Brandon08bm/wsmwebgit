package com.wsmweb.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wsmweb.entity.Bodega;
import com.wsmweb.utils.HibernateUtil;

public class BodegaHome {


	private String tabla=".bodega";


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Bodega findBy(String field, String value,String state){
		Bodega resultado = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT bodid, bodcodigo, bodnombre, bodestado, bodrojos, bodusucrea,"
					+ " bodusumod, bodfeccrea, bodfecmod  "
					+ "FROM "+schema+tabla;
			String where ="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				if(!field.equals("bodid")){
					where += " AND "+field.trim() + " = :P1";
					properties.put("p1",value);
				}
			}
			if(state != null && !state.equals("")){
				where += " AND bodestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString += (where.startsWith(" AND "))? " WHERE "+ where.substring(4): " WHERE " + where;
			}
			queryString +=" LIMIT 1";
			List result = (List<Bodega>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				while(((Iterator)resIterator).hasNext()){
					resultado = new Bodega();
					Object[] row = (Object[])resIterator.next();

					Integer bodid  = (Integer) row[0];
					String bodcodigo = (String) row[1];
					String bodnombre = (String) row[2];
					Boolean bodestado = ((Integer) row[3]).intValue()==1;
					Boolean bodrojos = ((Integer) row[4]).intValue()==1;
					String bodusucrea = (String) row[5];
					String bodusumod = (String) row[6];
					Date bodfeccrea = (Date) row[7];
					Date bodfecmod = (Date) row[8];

					resultado.setBodid(bodid);
					resultado.setBodcodigo(bodcodigo);
					resultado.setBodnombre(bodnombre);
					resultado.setBodestado(bodestado);
					resultado.setBodrojos(bodrojos);
					resultado.setBodusucrea(bodusucrea);
					resultado.setBodusumod(bodusumod);
					resultado.setBodfeccrea(bodfeccrea);
					resultado.setBodfecmod(bodfecmod);

				}
			}
			session.close();
			return resultado;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Bodega> findAll(String field, String value,String state,boolean like){
		List<Bodega> lista = new ArrayList<Bodega>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT bodid, bodcodigo, bodnombre, bodestado, bodrojos, bodusucrea,"
					+ " bodusumod, bodfeccrea, bodfecmod  "
					+ "FROM "+schema+tabla;
			String where="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				if(like){
					where += " AND "+field.trim()+" LIKE :p1";
					properties.put("p1", "%"+value+"%");
				}
				else {
					where += " AND "+field.trim()+" = :p1";
					properties.put("p1", value);
				}
			}
			if(state != null && !state.equals("")){
				where += " AND bodestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString +=(where.startsWith(" AND "))?" WHERE "+where.substring(4):" WHERE "+where;
			}
			if(field != null && !field.trim().equals("")){
				queryString += " ORDER BY "+ field;
			}else{
				queryString += " ORDER BY bodnombre";
			}

			List result = (List<Bodega>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				Bodega resultado = null;
				while(((Iterator)resIterator).hasNext()){
					resultado = new Bodega();
					Object[] row = (Object[])resIterator.next();

					Integer bodid  = (Integer) row[0];;
					String bodcodigo = (String) row[1];
					String bodnombre = (String) row[2];
					Boolean bodestado = ((Short) row[3]).intValue()==1;
					Boolean bodrojos = ((Short) row[4]).intValue()==1;
					String bodusucrea = (String) row[5];
					String bodusumod = (String) row[6];
					Date bodfeccrea = (Date) row[7];
					Date bodfecmod = (Date) row[8];

					resultado.setBodid(bodid);
					resultado.setBodcodigo(bodcodigo);
					resultado.setBodnombre(bodnombre);
					resultado.setBodestado(bodestado);
					resultado.setBodrojos(bodrojos);
					resultado.setBodusucrea(bodusucrea);
					resultado.setBodusumod(bodusumod);
					resultado.setBodfeccrea(bodfeccrea);
					resultado.setBodfecmod(bodfecmod);

					lista.add(resultado);
				}
			}
			session.close();
			return lista;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	public void save(Bodega obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString ="INSERT INTO "+schema+tabla
					+ " (bodcodigo, bodnombre, bodestado, bodrojos, bodusucrea, bodusumod, bodfeccrea, bodfecmod)"
					+ " VALUES (:p1,:p2,:p3,:p4,:p5,:p6,:p7,:p8 )";
			session.beginTransaction();
			Query query = session.createSQLQuery(queryString)
				.setParameter("p1", obj.getBodcodigo().toUpperCase().trim())
				.setParameter("p", obj.getBodnombre().toUpperCase().trim())
				.setParameter("p", (obj.getBodestado()?1:0))
				.setParameter("p", (obj.getBodrojos()?1:0))
				.setParameter("p", usuario.trim())
				.setParameter("p", usuario.trim())
				.setParameter("p", "now()")
				.setParameter("p", "now()");
			query.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void update(Bodega obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" UPDATE "+schema+tabla
					+" SET bodnombre='"+obj.getBodnombre().toUpperCase().trim()+"'," 
					+" bodestado ="+(obj.getBodestado()?1:0)+"," 
					+" bodrojos ="+(obj.getBodrojos()?1:0)+"," 
					+" bodusumod= '"+usuario.trim()+"',"
					+" bodfecmod= now() "
					+" WHERE bodid="+obj.getBodid()
					+ " AND bodcodigo='"+obj.getBodcodigo().toUpperCase().trim()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void delete(Bodega obj){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString= " DELETE FROM " +schema+tabla
					+" WHERE bodid="+obj.getBodid()
					+ " AND bodcodigo='"+obj.getBodcodigo().toUpperCase().trim()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		} catch (RuntimeException e) {
			session.close();
		}
	}


}
