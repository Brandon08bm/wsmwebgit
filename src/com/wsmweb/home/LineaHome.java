package com.wsmweb.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wsmweb.entity.Linea;
import com.wsmweb.utils.HibernateUtil;

public class LineaHome {

	private String tabla=".linea";


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Linea findBy(String field, String value,String state){
		Linea resultado = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT linid, lincodigo, linnombre, linestado, linusucrea, linusumod, "
					+ "linfeccrea, linfecmod FROM "+schema+tabla;
			String where ="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				where += " AND "+field.trim() + " = :p1'";
				properties.put("p1", value);
			}
			if(state != null && !state.equals("")){
				where += " AND linestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString += (where.startsWith(" AND "))? " WHERE "+ where.substring(4): " WHERE " + where;
			}
			queryString +=" LIMIT 1";
			List result = (List<Linea>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				while(((Iterator)resIterator).hasNext()){
					resultado = new Linea();
					Object[] row = (Object[])resIterator.next();

					Integer linid = (Integer) row[0];
					String lincodigo = (String) row[1];
					String linnombre = (String) row[2];
					Boolean linestado = ((Short) row[3]).intValue()==1;
					String linusucrea = (String) row[4];
					String linusumod = (String) row[5];
					Date linfeccrea = (Date) row[6];
					Date linfecmod = (Date) row[7];

					resultado.setLinid(linid);
					resultado.setLincodigo(lincodigo);
					resultado.setLinnombre(linnombre);
					resultado.setLinestado(linestado);
					resultado.setLinusucrea(linusucrea);
					resultado.setLinusumod(linusumod);
					resultado.setLinfeccrea(linfeccrea);
					resultado.setLinfecmod(linfecmod);


				}
			}
			session.close();
			return resultado;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Linea> findAll(String field, String value,String state,boolean like){
		List<Linea> lista = new ArrayList<Linea>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT linid, lincodigo, linnombre, linestado, linusucrea, linusumod, "
					+ "linfeccrea, linfecmod FROM "+schema+tabla;
			String where="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				if(like){
					where += " AND "+field.trim()+" LIKE :p1";
					properties.put("p1", "%"+value+"%");
				}else{
					where += " AND "+field.trim()+"=:p1";
					properties.put("p1", value);
				}
			}
			if(state != null && !state.equals("")){
				where += " AND linestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString +=(where.startsWith(" AND "))?" WHERE "+where.substring(4):" WHERE "+where;
			}
			if(field != null && !field.trim().equals("")){
				queryString += " ORDER BY "+ field;
			}else{
				queryString += " ORDER BY linnombre";
			}
			List result = (List<Linea>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				Linea resultado = null;
				while(((Iterator)resIterator).hasNext()){
					resultado = new Linea();
					Object[] row = (Object[])resIterator.next();

					Integer linid = (Integer) row[0];
					String lincodigo = (String) row[1];
					String linnombre = (String) row[2];
					Boolean linestado = ((Short) row[3]).intValue()==1;
					String linusucrea = (String) row[4];
					String linusumod = (String) row[5];
					Date linfeccrea = (Date) row[6];
					Date linfecmod = (Date) row[7];

					resultado.setLinid(linid);
					resultado.setLincodigo(lincodigo);
					resultado.setLinnombre(linnombre);
					resultado.setLinestado(linestado);
					resultado.setLinusucrea(linusucrea);
					resultado.setLinusumod(linusumod);
					resultado.setLinfeccrea(linfeccrea);
					resultado.setLinfecmod(linfecmod);

					lista.add(resultado);
				}
			}
			session.close();
			return lista;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	public void save(Linea obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" INSERT INTO "+schema+tabla
					+ " (lincodigo, linnombre, linestado, linusucrea, linusumod, linfeccrea, linfecmod)"
					+ " VALUES ( "
					+"'"+obj.getLincodigo().toUpperCase().trim()+"',"
					+"'"+obj.getLinnombre().toUpperCase().trim()+"',"
					+(obj.getLinestado()?1:0)+","
					+"'"+ usuario.trim() +"',"
					+"'"+ usuario.trim() +"',"
					+ " now(),"
					+ " now() )";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void update(Linea obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" UPDATE "+schema+tabla
					+" SET linnombre='"+obj.getLinnombre().toUpperCase().trim()+"'," 
					+" linestado ="+(obj.getLinestado()?1:0)+"," 
					+" bodusumod= '"+usuario.trim()+"',"
					+" bodfecmod= now() "
					+" WHERE linid="+obj.getLinid()
					+ " AND lincodigo='"+obj.getLincodigo().toUpperCase().trim()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void delete(Linea obj){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString= " DELETE FROM " +schema+tabla
					+" WHERE linid="+obj.getLinid()
					+ " AND lincodigo='"+obj.getLincodigo().toUpperCase().trim()+"'";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		} catch (RuntimeException e) {
			session.close();
		}
	}



}
