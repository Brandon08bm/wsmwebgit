package com.wsmweb.home;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.wsmweb.entity.Bodega;
import com.wsmweb.entity.Pais;
import com.wsmweb.utils.HibernateUtil;

public class PaisHome {

	private String tabla=".pais";


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Pais findBy(String field, String value,String state){
		Pais resultado = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT paiid, paicodigo, painombre, paiestado, "
					+ " paiusucrea, paiusumod,  paifeccrea, paifecmod FROM "+schema+tabla;
			String where ="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				where += " AND "+field.trim() + " = :p1";
				properties.put("p1", value);
			}
			if(state != null && !state.equals("")){
				where += " AND paiestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString += (where.startsWith(" AND "))? " WHERE "+ where.substring(4): " WHERE " + where;
			}
			queryString +=" LIMIT 1";
			List result = (List<Bodega>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				while(((Iterator)resIterator).hasNext()){
					resultado = new Pais();
					Object[] row = (Object[])resIterator.next();

					Integer paiid = (Integer) row[0];
					Integer paicodigo = (Integer) row[1];
					String painombre = (String) row[2];
					Boolean paiestado = ((Short) row[3]).intValue()==1;
					String paiusucrea = (String) row[4];
					String paiusumod = (String) row[5];
					Date paifeccrea = (Date) row[6];
					Date paifecmod = (Date) row[7];

					resultado.setPaiid(paiid);
					resultado.setPaicodigo(paicodigo);
					resultado.setPainombre(painombre);
					resultado.setPaiestado(paiestado);
					resultado.setPaiusucrea(paiusucrea);
					resultado.setPaiusumod(paiusumod);
					resultado.setPaifeccrea(paifeccrea);
					resultado.setPaifecmod(paifecmod);

				}
			}
			session.close();
			return resultado;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Pais> findAll(String field, String value,String state,boolean like){
		List<Pais> lista = new ArrayList<Pais>();
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema =  HibernateUtil.getSchema();
		final Map<String, Object> properties = new HashMap<>();
		try {
			String queryString= " SELECT paiid, paicodigo, painombre, paiestado, "
					+ " paiusucrea, paiusumod,  paifeccrea, paifecmod FROM "+schema+tabla;
			String where="";
			if((field != null && !field.equals("")) && (value !=null && !value.equals(""))){
				if(!field.equals("paicodigo")){
					if(like){
						where += " AND "+field.trim()+" LIKE :p1";
						properties.put("p1", "%"+value+"%");
					}
				}else{
					where += " AND "+field.trim()+"= :p1";
					properties.put("p1", Integer.parseInt(value));
				}
			}
			if(state != null && !state.equals("")){
				where += " AND paiestado = :p2";
				properties.put("p2", Integer.parseInt(state));
			}
			if(where.trim()!=""){
				queryString +=(where.startsWith(" AND "))?" WHERE "+where.substring(4):" WHERE "+where;
			}
			if(field != null && !field.trim().equals("")){
				queryString += " ORDER BY "+ field.trim();
			}else{
				queryString += " ORDER BY painombre";
			}
			List result = (List<Pais>)session.createSQLQuery(queryString).setProperties(properties).list();
			if(result!=null && result.size()>0){
				Iterator resIterator = result.iterator();
				Pais resultado = null;
				while(((Iterator)resIterator).hasNext()){
					resultado = new Pais();
					Object[] row = (Object[])resIterator.next();

					Integer paiid = (Integer) row[0];
					Integer paicodigo = (Integer) row[1];
					String painombre = (String) row[2];
					Boolean paiestado = ((Short) row[3]).intValue()==1;
					String paiusucrea = (String) row[4];
					String paiusumod = (String) row[5];
					Date paifeccrea = (Date) row[6];
					Date paifecmod = (Date) row[7];

					resultado.setPaiid(paiid);
					resultado.setPaicodigo(paicodigo);
					resultado.setPainombre(painombre);
					resultado.setPaiestado(paiestado);
					resultado.setPaiusucrea(paiusucrea);
					resultado.setPaiusumod(paiusumod);
					resultado.setPaifeccrea(paifeccrea);
					resultado.setPaifecmod(paifecmod);

					lista.add(resultado);
				}
			}
			session.close();
			return lista;
		} catch (RuntimeException e) {
			session.close();
			throw e;
		}
	}

	public void save(Pais obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" INSERT INTO "+schema+tabla
					+ "( paicodigo, painombre, paiestado, paiusucrea, paiusumod, paifeccrea, paifecmod)"
					+ " VALUES ( "
					+"'"+obj.getPaicodigo()+","
					+"'"+obj.getPainombre().toUpperCase()+"',"
					+(obj.getPaiestado()?1:0)+","
					+"'"+ usuario+"',"
					+"'"+ usuario +"',"
					+ " now(),"
					+ " now() )";
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void update(Pais obj){
		//		String usuario = (String) FacesUtils.getSessionParameter("usucodigo");
		String usuario = "QS";
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString =" UPDATE "+schema+tabla
					+" SET painombre='"+obj.getPainombre().toUpperCase()+"'," 
					+" paiestado ="+(obj.getPaiestado()?1:0)+"," 
					+" paiusumod= '"+usuario+"',"
					+" paifecmod= now() "
					+" WHERE paiid="+obj.getPaiid()
					+ " AND paicodigo="+obj.getPaicodigo();
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		}catch (RuntimeException e) {
			session.close();
		}
	}

	public void delete(Pais obj){
		Session session = HibernateUtil.getSessionFactory().openSession();
		String schema = HibernateUtil.getSchema();
		try {
			String queryString= " DELETE FROM " +schema+tabla
					+" WHERE paiid="+obj.getPaiid()
					+ " AND mpaicodigo="+obj.getPaicodigo();
			session.beginTransaction();
			SQLQuery sqlQuery = session.createSQLQuery(queryString);
			sqlQuery.executeUpdate();
			session.getTransaction().commit();
			session.close();
		} catch (RuntimeException e) {
			session.close();
		}
	}
}
