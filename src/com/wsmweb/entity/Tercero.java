package com.wsmweb.entity;
// Generated 17/05/2017 04:31:36 PM by Hibernate Tools 5.2.0.CR1

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Tercero generated by hbm2java
 */
@SuppressWarnings("serial")
public class Tercero implements java.io.Serializable {

	@JsonProperty(value = "uid")
	private Integer terid;
	
	@JsonProperty(value = "tpd")
	private String tertipodoc;
	
	@JsonProperty(value = "doc")
	private String terdocumto;
	
	@JsonProperty(value = "no1")
	private String ternombre1;
	
	@JsonProperty(value = "no2")
	private String ternombre2;
	
	@JsonProperty(value = "ap1")
	private String terapelli1;
	
	@JsonProperty(value = "ap2")
	private String terapelli2;
	
	@JsonProperty(value = "gnr")
	private Integer tergenero;
	
	@JsonProperty(value = "fcn")
	private Date terfecnac;
	
	@JsonProperty(value = "dre")
	private String terdirecci;
	
	@JsonProperty(value = "icu")
	private Integer terciudad;
	
	@JsonProperty(value = "cin")
	private String terciudadnom;
	
	@JsonProperty(value = "te1")
	private String tertelefon1;
	
	@JsonProperty(value = "t2")
	private String tertelefon2;
	
	@JsonProperty(value = "te3")
	private String tertelefon3;
	
	@JsonProperty(value = "fax")
	private String terfax;
	
	@JsonProperty(value = "ema")
	private String tercorreo;
	
	@JsonProperty(value = "not")
	private String ternota;
	
	@JsonProperty(value = "std")
	private Boolean terestado;
	
	@JsonProperty(value = "auc")
	private String terusucrea;
	
	@JsonProperty(value = "aum")
	private String terusumod;
	
	@JsonProperty(value = "afc")
	private Date terfeccrea;
	
	@JsonProperty(value = "afm")
	private Date terfecmod;
	

	public Tercero() {
	}


	public Tercero(Integer terid, String tertipodoc, String terdocumto, String ternombre1, String ternombre2,
			String terapelli1, String terapelli2, Integer tergenero, Date terfecnac, String terdirecci,
			Integer terciudad, String terciudadnom, String tertelefon1, String tertelefon2, String tertelefon3,
			String terfax, String tercorreo, String ternota, Boolean terestado, String terusucrea, String terusumod,
			Date terfeccrea, Date terfecmod) {
		this.terid = terid;
		this.tertipodoc = tertipodoc;
		this.terdocumto = terdocumto;
		this.ternombre1 = ternombre1;
		this.ternombre2 = ternombre2;
		this.terapelli1 = terapelli1;
		this.terapelli2 = terapelli2;
		this.tergenero = tergenero;
		this.terfecnac = terfecnac;
		this.terdirecci = terdirecci;
		this.terciudad = terciudad;
		this.terciudadnom = terciudadnom;
		this.tertelefon1 = tertelefon1;
		this.tertelefon2 = tertelefon2;
		this.tertelefon3 = tertelefon3;
		this.terfax = terfax;
		this.tercorreo = tercorreo;
		this.ternota = ternota;
		this.terestado = terestado;
		this.terusucrea = terusucrea;
		this.terusumod = terusumod;
		this.terfeccrea = terfeccrea;
		this.terfecmod = terfecmod;
	}


	public Integer getTerid() {
		return terid;
	}


	public void setTerid(Integer terid) {
		this.terid = terid;
	}


	public String getTertipodoc() {
		return tertipodoc;
	}


	public void setTertipodoc(String tertipodoc) {
		this.tertipodoc = tertipodoc;
	}


	public String getTerdocumto() {
		return terdocumto;
	}


	public void setTerdocumto(String terdocumto) {
		this.terdocumto = terdocumto;
	}


	public String getTernombre1() {
		return ternombre1;
	}


	public void setTernombre1(String ternombre1) {
		this.ternombre1 = ternombre1;
	}


	public String getTernombre2() {
		return ternombre2;
	}


	public void setTernombre2(String ternombre2) {
		this.ternombre2 = ternombre2;
	}


	public String getTerapelli1() {
		return terapelli1;
	}


	public void setTerapelli1(String terapelli1) {
		this.terapelli1 = terapelli1;
	}


	public String getTerapelli2() {
		return terapelli2;
	}


	public void setTerapelli2(String terapelli2) {
		this.terapelli2 = terapelli2;
	}


	public Integer getTergenero() {
		return tergenero;
	}


	public void setTergenero(Integer tergenero) {
		this.tergenero = tergenero;
	}


	public Date getTerfecnac() {
		return terfecnac;
	}


	public void setTerfecnac(Date terfecnac) {
		this.terfecnac = terfecnac;
	}


	public String getTerdirecci() {
		return terdirecci;
	}


	public void setTerdirecci(String terdirecci) {
		this.terdirecci = terdirecci;
	}


	public Integer getTerciudad() {
		return terciudad;
	}


	public void setTerciudad(Integer terciudad) {
		this.terciudad = terciudad;
	}


	public String getTerciudadnom() {
		return terciudadnom;
	}


	public void setTerciudadnom(String terciudadnom) {
		this.terciudadnom = terciudadnom;
	}


	public String getTertelefon1() {
		return tertelefon1;
	}


	public void setTertelefon1(String tertelefon1) {
		this.tertelefon1 = tertelefon1;
	}


	public String getTertelefon2() {
		return tertelefon2;
	}


	public void setTertelefon2(String tertelefon2) {
		this.tertelefon2 = tertelefon2;
	}


	public String getTertelefon3() {
		return tertelefon3;
	}


	public void setTertelefon3(String tertelefon3) {
		this.tertelefon3 = tertelefon3;
	}


	public String getTerfax() {
		return terfax;
	}


	public void setTerfax(String terfax) {
		this.terfax = terfax;
	}


	public String getTercorreo() {
		return tercorreo;
	}


	public void setTercorreo(String tercorreo) {
		this.tercorreo = tercorreo;
	}


	public String getTernota() {
		return ternota;
	}


	public void setTernota(String ternota) {
		this.ternota = ternota;
	}


	public Boolean getTerestado() {
		return terestado;
	}


	public void setTerestado(Boolean terestado) {
		this.terestado = terestado;
	}


	public String getTerusucrea() {
		return terusucrea;
	}


	public void setTerusucrea(String terusucrea) {
		this.terusucrea = terusucrea;
	}


	public String getTerusumod() {
		return terusumod;
	}


	public void setTerusumod(String terusumod) {
		this.terusumod = terusumod;
	}


	public Date getTerfeccrea() {
		return terfeccrea;
	}


	public void setTerfeccrea(Date terfeccrea) {
		this.terfeccrea = terfeccrea;
	}


	public Date getTerfecmod() {
		return terfecmod;
	}


	public void setTerfecmod(Date terfecmod) {
		this.terfecmod = terfecmod;
	}


	@Override
	public String toString() {
		return "Tercero [terid=" + terid + ", tertipodoc=" + tertipodoc + ", terdocumto=" + terdocumto + ", ternombre1="
				+ ternombre1 + ", ternombre2=" + ternombre2 + ", terapelli1=" + terapelli1 + ", terapelli2="
				+ terapelli2 + ", tergenero=" + tergenero + ", terfecnac=" + terfecnac + ", terdirecci=" + terdirecci
				+ ", terciudad=" + terciudad + ", terciudadnom=" + terciudadnom + ", tertelefon1=" + tertelefon1
				+ ", tertelefon2=" + tertelefon2 + ", tertelefon3=" + tertelefon3 + ", terfax=" + terfax
				+ ", tercorreo=" + tercorreo + ", ternota=" + ternota + ", terestado=" + terestado + ", terusucrea="
				+ terusucrea + ", terusumod=" + terusumod + ", terfeccrea=" + terfeccrea + ", terfecmod=" + terfecmod
				+ "]";
	}

}
