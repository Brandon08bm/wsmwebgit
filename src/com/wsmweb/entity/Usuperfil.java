package com.wsmweb.entity;
// Generated 17/05/2017 04:31:36 PM by Hibernate Tools 5.2.0.CR1

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Usuperfil generated by hbm2java
 */
@SuppressWarnings("serial")
public class Usuperfil implements java.io.Serializable {

	@JsonProperty(value = "uid")
	private Integer upfid;

	@JsonProperty(value = "usu")
	private Integer upfusuario;

	@JsonProperty(value = "usn")
	private String upfusuarionom;

	@JsonProperty(value = "prf")
	private Integer upfperfil;

	@JsonProperty(value = "prn")
	private String upfperfilnom;

	@JsonProperty(value = "auc")
	private String upfusucrea;

	@JsonProperty(value = "aum")
	private String upfusumod;

	@JsonProperty(value = "afc")
	private Date upffeccrea;

	@JsonProperty(value = "afm")
	private Date upffecmod;


	public Usuperfil() {
	}


	public Usuperfil(Integer upfid, Integer upfusuario, String upfusuarionom, Integer upfperfil, String upfperfilnom,
			String upfusucrea, String upfusumod, Date upffeccrea, Date upffecmod) {
		this.upfid = upfid;
		this.upfusuario = upfusuario;
		this.upfusuarionom = upfusuarionom;
		this.upfperfil = upfperfil;
		this.upfperfilnom = upfperfilnom;
		this.upfusucrea = upfusucrea;
		this.upfusumod = upfusumod;
		this.upffeccrea = upffeccrea;
		this.upffecmod = upffecmod;
	}


	public Integer getUpfid() {
		return upfid;
	}


	public void setUpfid(Integer upfid) {
		this.upfid = upfid;
	}


	public Integer getUpfusuario() {
		return upfusuario;
	}


	public void setUpfusuario(Integer upfusuario) {
		this.upfusuario = upfusuario;
	}


	public String getUpfusuarionom() {
		return upfusuarionom;
	}


	public void setUpfusuarionom(String upfusuarionom) {
		this.upfusuarionom = upfusuarionom;
	}


	public Integer getUpfperfil() {
		return upfperfil;
	}


	public void setUpfperfil(Integer upfperfil) {
		this.upfperfil = upfperfil;
	}


	public String getUpfperfilnom() {
		return upfperfilnom;
	}


	public void setUpfperfilnom(String upfperfilnom) {
		this.upfperfilnom = upfperfilnom;
	}


	public String getUpfusucrea() {
		return upfusucrea;
	}


	public void setUpfusucrea(String upfusucrea) {
		this.upfusucrea = upfusucrea;
	}


	public String getUpfusumod() {
		return upfusumod;
	}


	public void setUpfusumod(String upfusumod) {
		this.upfusumod = upfusumod;
	}


	public Date getUpffeccrea() {
		return upffeccrea;
	}


	public void setUpffeccrea(Date upffeccrea) {
		this.upffeccrea = upffeccrea;
	}


	public Date getUpffecmod() {
		return upffecmod;
	}


	public void setUpffecmod(Date upffecmod) {
		this.upffecmod = upffecmod;
	}


	@Override
	public String toString() {
		return "Usuperfil [upfid=" + upfid + ", upfusuario=" + upfusuario + ", upfusuarionom=" + upfusuarionom
				+ ", upfperfil=" + upfperfil + ", upfperfilnom=" + upfperfilnom + ", upfusucrea=" + upfusucrea
				+ ", upfusumod=" + upfusumod + ", upffeccrea=" + upffeccrea + ", upffecmod=" + upffecmod + "]";
	}

}
