package com.wsmweb.entity;
// Generated 17/05/2017 04:31:36 PM by Hibernate Tools 5.2.0.CR1

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Empleado generated by hbm2java
 */
@SuppressWarnings("serial")
public class Empleado implements java.io.Serializable {

	@JsonProperty(value = "uid")
	private Integer empid;

	@JsonProperty(value = "trc")
	private Integer emptercero;

	@JsonProperty(value = "trn")
	private String empterceronom;

	@JsonProperty(value = "crg")
	private String empcargo;

	@JsonProperty(value = "slr")
	private Double empsalario;

	@JsonProperty(value = "fng")
	private Date empfecing;

	@JsonProperty(value = "fct")
	private Date empfecret;

	@JsonProperty(value = "std")
	private Boolean empestado;

	@JsonProperty(value = "auc")
	private String empusucrea;

	@JsonProperty(value = "aum")
	private String empusumod;

	@JsonProperty(value = "afc")
	private Date empfeccrea;

	@JsonProperty(value = "afm")
	private Date empfecmod;


	public Empleado() {
	}


	public Empleado(Integer empid, Integer emptercero, String empterceronom, String empcargo, Double empsalario,
			Date empfecing, Date empfecret, Boolean empestado, String empusucrea, String empusumod, Date empfeccrea,
			Date empfecmod) {
		this.empid = empid;
		this.emptercero = emptercero;
		this.empterceronom = empterceronom;
		this.empcargo = empcargo;
		this.empsalario = empsalario;
		this.empfecing = empfecing;
		this.empfecret = empfecret;
		this.empestado = empestado;
		this.empusucrea = empusucrea;
		this.empusumod = empusumod;
		this.empfeccrea = empfeccrea;
		this.empfecmod = empfecmod;
	}


	public Integer getEmpid() {
		return empid;
	}


	public void setEmpid(Integer empid) {
		this.empid = empid;
	}


	public Integer getEmptercero() {
		return emptercero;
	}


	public void setEmptercero(Integer emptercero) {
		this.emptercero = emptercero;
	}


	public String getEmpterceronom() {
		return empterceronom;
	}


	public void setEmpterceronom(String empterceronom) {
		this.empterceronom = empterceronom;
	}


	public String getEmpcargo() {
		return empcargo;
	}


	public void setEmpcargo(String empcargo) {
		this.empcargo = empcargo;
	}


	public Double getEmpsalario() {
		return empsalario;
	}


	public void setEmpsalario(Double empsalario) {
		this.empsalario = empsalario;
	}


	public Date getEmpfecing() {
		return empfecing;
	}


	public void setEmpfecing(Date empfecing) {
		this.empfecing = empfecing;
	}


	public Date getEmpfecret() {
		return empfecret;
	}


	public void setEmpfecret(Date empfecret) {
		this.empfecret = empfecret;
	}


	public Boolean getEmpestado() {
		return empestado;
	}


	public void setEmpestado(Boolean empestado) {
		this.empestado = empestado;
	}


	public String getEmpusucrea() {
		return empusucrea;
	}


	public void setEmpusucrea(String empusucrea) {
		this.empusucrea = empusucrea;
	}


	public String getEmpusumod() {
		return empusumod;
	}


	public void setEmpusumod(String empusumod) {
		this.empusumod = empusumod;
	}


	public Date getEmpfeccrea() {
		return empfeccrea;
	}


	public void setEmpfeccrea(Date empfeccrea) {
		this.empfeccrea = empfeccrea;
	}


	public Date getEmpfecmod() {
		return empfecmod;
	}


	public void setEmpfecmod(Date empfecmod) {
		this.empfecmod = empfecmod;
	}

	@Override
	public String toString() {
		return "Empleado [empid=" + empid + ", emptercero=" + emptercero + ", empterceronom=" + empterceronom
				+ ", empcargo=" + empcargo + ", empsalario=" + empsalario + ", empfecing=" + empfecing + ", empfecret="
				+ empfecret + ", empestado=" + empestado + ", empusucrea=" + empusucrea + ", empusumod=" + empusumod
				+ ", empfeccrea=" + empfeccrea + ", empfecmod=" + empfecmod + "]";
	}

}
