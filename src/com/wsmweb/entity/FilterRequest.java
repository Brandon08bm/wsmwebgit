package com.wsmweb.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FilterRequest {

	
	@JsonProperty(value = "f")
	private String field;
	
	@JsonProperty(value = "v")
	private String value;
	
	@JsonProperty(value = "s")
	private String state;
	

	public FilterRequest() {
	}

	public FilterRequest(String field, String value, String state) {
		this.field = field;
		this.value = value;
		this.state = state;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "DatosFiltro [field=" + field + ", value=" + value + ", state=" + state + "]";
	}
}
