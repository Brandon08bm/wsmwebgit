package com.wsmweb.entity;
// Generated 17/05/2017 04:31:36 PM by Hibernate Tools 5.2.0.CR1

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Empresa generated by hbm2java
 */
@SuppressWarnings("serial")
public class Empresa implements java.io.Serializable {

	@JsonProperty(value = "uid")
	private Integer empid;

	@JsonProperty(value = "cdg")
	private String empcodigo;

	@JsonProperty(value = "nit")
	private String empnit;

	@JsonProperty(value = "nmb")
	private String empnombre;

	@JsonProperty(value = "tlf")
	private String emptelefono;

	@JsonProperty(value = "dcc")
	private String empdireccion;

	@JsonProperty(value = "ppt")
	private String emppropietario;

	@JsonProperty(value = "cdd")
	private Integer empciudad;

	@JsonProperty(value = "cdn")
	private String empciudadnom;

	@JsonProperty(value = "mod")
	private String empmoneda;

	@JsonProperty(value = "fci")
	private Date empfecini;

	@JsonProperty(value = "fcf")
	private Date empfecfin;

	@JsonProperty(value = "rgm")
	private Integer empregim;

	@JsonProperty(value = "rtt")
	private Integer emprtefte;

	@JsonProperty(value = "nif")
	private Integer empniif;

	@JsonProperty(value = "std")
	private Boolean empestado;

	@JsonProperty(value = "auc")
	private String empusucrea;

	@JsonProperty(value = "aum")
	private String empusumod;

	@JsonProperty(value = "afc")
	private Date empfeccrea;

	@JsonProperty(value = "afm")
	private Date empfecmod;


	public Empresa() {
	}


	public Empresa(Integer empid, String empcodigo, String empnit, String empnombre, String emptelefono,
			String empdireccion, String emppropietario, Integer empciudad, String empciudadnom, String empmoneda,
			Date empfecini, Date empfecfin, Integer empregim, Integer emprtefte, Integer empniif, Boolean empestado,
			String empusucrea, String empusumod, Date empfeccrea, Date empfecmod) {
		this.empid = empid;
		this.empcodigo = empcodigo;
		this.empnit = empnit;
		this.empnombre = empnombre;
		this.emptelefono = emptelefono;
		this.empdireccion = empdireccion;
		this.emppropietario = emppropietario;
		this.empciudad = empciudad;
		this.empciudadnom = empciudadnom;
		this.empmoneda = empmoneda;
		this.empfecini = empfecini;
		this.empfecfin = empfecfin;
		this.empregim = empregim;
		this.emprtefte = emprtefte;
		this.empniif = empniif;
		this.empestado = empestado;
		this.empusucrea = empusucrea;
		this.empusumod = empusumod;
		this.empfeccrea = empfeccrea;
		this.empfecmod = empfecmod;
	}


	public Integer getEmpid() {
		return empid;
	}


	public void setEmpid(Integer empid) {
		this.empid = empid;
	}


	public String getEmpcodigo() {
		return empcodigo;
	}


	public void setEmpcodigo(String empcodigo) {
		this.empcodigo = empcodigo;
	}


	public String getEmpnit() {
		return empnit;
	}


	public void setEmpnit(String empnit) {
		this.empnit = empnit;
	}


	public String getEmpnombre() {
		return empnombre;
	}


	public void setEmpnombre(String empnombre) {
		this.empnombre = empnombre;
	}


	public String getEmptelefono() {
		return emptelefono;
	}


	public void setEmptelefono(String emptelefono) {
		this.emptelefono = emptelefono;
	}


	public String getEmpdireccion() {
		return empdireccion;
	}


	public void setEmpdireccion(String empdireccion) {
		this.empdireccion = empdireccion;
	}


	public String getEmppropietario() {
		return emppropietario;
	}


	public void setEmppropietario(String emppropietario) {
		this.emppropietario = emppropietario;
	}


	public Integer getEmpciudad() {
		return empciudad;
	}


	public void setEmpciudad(Integer empciudad) {
		this.empciudad = empciudad;
	}


	public String getEmpciudadnom() {
		return empciudadnom;
	}


	public void setEmpciudadnom(String empciudadnom) {
		this.empciudadnom = empciudadnom;
	}


	public String getEmpmoneda() {
		return empmoneda;
	}


	public void setEmpmoneda(String empmoneda) {
		this.empmoneda = empmoneda;
	}


	public Date getEmpfecini() {
		return empfecini;
	}


	public void setEmpfecini(Date empfecini) {
		this.empfecini = empfecini;
	}


	public Date getEmpfecfin() {
		return empfecfin;
	}


	public void setEmpfecfin(Date empfecfin) {
		this.empfecfin = empfecfin;
	}


	public Integer getEmpregim() {
		return empregim;
	}


	public void setEmpregim(Integer empregim) {
		this.empregim = empregim;
	}


	public Integer getEmprtefte() {
		return emprtefte;
	}


	public void setEmprtefte(Integer emprtefte) {
		this.emprtefte = emprtefte;
	}


	public Integer getEmpniif() {
		return empniif;
	}


	public void setEmpniif(Integer empniif) {
		this.empniif = empniif;
	}


	public Boolean getEmpestado() {
		return empestado;
	}


	public void setEmpestado(Boolean empestado) {
		this.empestado = empestado;
	}


	public String getEmpusucrea() {
		return empusucrea;
	}


	public void setEmpusucrea(String empusucrea) {
		this.empusucrea = empusucrea;
	}


	public String getEmpusumod() {
		return empusumod;
	}


	public void setEmpusumod(String empusumod) {
		this.empusumod = empusumod;
	}


	public Date getEmpfeccrea() {
		return empfeccrea;
	}


	public void setEmpfeccrea(Date empfeccrea) {
		this.empfeccrea = empfeccrea;
	}


	public Date getEmpfecmod() {
		return empfecmod;
	}


	public void setEmpfecmod(Date empfecmod) {
		this.empfecmod = empfecmod;
	}


	@Override
	public String toString() {
		return "Empresa [empid=" + empid + ", empcodigo=" + empcodigo + ", empnit=" + empnit + ", empnombre="
				+ empnombre + ", emptelefono=" + emptelefono + ", empdireccion=" + empdireccion + ", emppropietario="
				+ emppropietario + ", empciudad=" + empciudad + ", empciudadnom=" + empciudadnom + ", empmoneda="
				+ empmoneda + ", empfecini=" + empfecini + ", empfecfin=" + empfecfin + ", empregim=" + empregim
				+ ", emprtefte=" + emprtefte + ", empniif=" + empniif + ", empestado=" + empestado + ", empusucrea="
				+ empusucrea + ", empusumod=" + empusumod + ", empfeccrea=" + empfeccrea + ", empfecmod=" + empfecmod
				+ "]";
	}

}
